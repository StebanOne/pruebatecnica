
package com.organitiempo.rest;

import javax.persistence.*;

@Entity @Table(name="persona")
public class Persona {
    @Id @Column @GeneratedValue(strategy=GenerationType.IDENTITY) private int id;
    @Column private Integer cod;
    @Column private String email;
    @Column private String comment;
    @Column private String pc;
    @Column private String fecha;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


}
